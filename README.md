## Marvel Heroes

The application lists and detail the Marvel heroes.
All the data is provided by Marvel's developer API.


---

## Installed pods
1. Alamofire - URL Requests
2. AlamofireImage - Image download

At this project I did prefer to use Alamofire and AlamofireImage instead of URLSession.

## Run the tests
You can execute the tests from command line using the Fastlane, through the lane `test`. Open the terminal at project root folder and put:
``` fastlane test
```
