//
//  MainViewModelTests.swift
//  marvel-heroesTests
//
//  Created by Pedro Veloso on 12/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import Foundation
import XCTest
@testable import marvel_heroes

class MainViewModelTests: XCTestCase {
    var viewModel: MainViewModel? {
        didSet {
            self.view = MockView()
            viewModel?.set(view: self.view!)
        }
    }
    private var view: MockView?
    private var coordinator: MockCoordinator?
    private var repository: MockRepository?
    
    override func setUp() {
        repository = MockRepository()
        coordinator = MockCoordinator()
        viewModel = MainViewModel(repository: repository!, coordinator: coordinator!)
    }
    
    private func fillHeroesArray() {
        repository?.responseType = .success
        viewModel?.fetch()
    }
    
    func testFetchWhenIsFetchingIsTrue() {
        viewModel?.isFetching = true
        viewModel?.fetch()
        
        XCTAssertFalse(repository!.fetchWasCalled, "fetch() should not be called at \(#function)")
    }
    
    func testFetchWhenSuccess() {
        let initialFetchPage = viewModel!.fetchPage
        fillHeroesArray()
        
        XCTAssertEqual(viewModel?.heroes, repository?.successResponse.results, "Heroes should be loaded with results from Mocked Json at \(#function)")
        XCTAssertTrue(view!.reloadDataWasCalled, "view.reloadData() should be called at \(#function)")
        XCTAssertFalse(viewModel!.isFetching, "isFetching should be setted to false")
        XCTAssertGreaterThan(viewModel!.fetchPage, initialFetchPage)
    }
    
    func testFetchWhenFailure() {
        repository?.responseType = .failure
        let initialFetchPage = viewModel!.fetchPage
        view?.alertMessageToBeSetted = NSError.generalError.domain
        viewModel?.fetch()
        
        XCTAssertFalse(view!.reloadDataWasCalled, "reloadData() should not be called at \(#function)")
        XCTAssertFalse(viewModel!.isFetching, "isFetching should be setted to false")
        XCTAssertEqual(viewModel!.fetchPage, initialFetchPage)
        
        XCTAssertTrue(view!.showAlertWasCalled, "showAlert(message:) should be called at \(#function)")
    }
    
    func testDidSelectHero() {
        fillHeroesArray()
        viewModel?.didSelectHero(at: 0)
        
        XCTAssertTrue(coordinator!.pushWasCalled, "push(viewController:) should be called at \(#function)")
    }
    
    func testShouldFetchWhenHasNoHeroes() {
        let shouldFetch = viewModel!.shouldFetch(for: [IndexPath(item: 1, section: 1)])
        XCTAssertFalse(shouldFetch)
    }
    
    func testShouldFetchWhenHasNoIndexPath() {
        fillHeroesArray()
        let shouldFetch = viewModel!.shouldFetch(for: [])
        XCTAssertFalse(shouldFetch)
    }
    
    func testShouldFetchWhenIndexIsLessThanNumberOfHeroes() {
        fillHeroesArray()
        let indexPath = IndexPath(row: -1, section: 1)
        let shouldFetch = viewModel!.shouldFetch(for: [indexPath])
        XCTAssertFalse(shouldFetch)
    }
    
    func testShouldFetchWhenIndexIsGreaterThanNumberOfHeroesAndLessThanTotalHeroesApi() {
        fillHeroesArray()
        let indexPath = IndexPath(row: 1, section: 1)
        let shouldFetch = viewModel!.shouldFetch(for: [indexPath])
        XCTAssertTrue(shouldFetch)
    }
    
}

// MARK: - MockRepository
extension MainViewModelTests {
    private enum ResponseType {
        case success, failure, none
    }
    
    private class MockRepository: RepositoryDelegate {
        
        var responseType: ResponseType = .none
        var fetchWasCalled = false
        
        let successResponse = MockLoader<ResponseWrapper<Hero>>.loadMock()
        
        func fetch(offset: Int, success: @escaping HeroesListSuccess, failure: @escaping Failure) {
            fetchWasCalled = true
            switch responseType {
            case .success:
                success(successResponse)
            case .failure:
                failure(NSError.generalError)
            default:
                break
            }
        }
    }
}

//MARK: - MockCoordinator
extension MainViewModelTests {
    private class MockCoordinator: CoordinatorDelegate {
        
        var pushWasCalled: Bool = false
        
        func push(viewController: UIViewController) {
            XCTAssertTrue(viewController is DetailViewController, "viewController should be a DetailViewController instance")
            pushWasCalled = true
        }
        
    }
}

//MARK: - MockView
extension MainViewModelTests {
    private class MockView: MainViewControllerDelegate {
        var reloadDataWasCalled = false
        var showAlertWasCalled = false
        
        var alertMessageToBeSetted: String?
        
        func reloadData() {
            reloadDataWasCalled = true
        }
        
        func showAlert(message: String) {
            XCTAssertEqual(message, alertMessageToBeSetted)
            showAlertWasCalled = true
        }
        
    }
}
