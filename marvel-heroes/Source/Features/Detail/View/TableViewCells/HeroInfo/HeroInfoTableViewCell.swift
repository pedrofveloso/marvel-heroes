//
//  HeroInfoTableViewCell.swift
//  marvel-heroes
//
//  Created by Pedro Veloso on 11/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit
import AlamofireImage

class HeroInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var thumbnailImageView: UIImageView! {
        didSet {
            thumbnailImageView.layer.cornerRadius = thumbnailImageView.frame.height/2
            thumbnailImageView.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.font = UIFont.preferredFont(forTextStyle: .headline)
        }
    }
    @IBOutlet weak var modifiedLabel: UILabel!{
        didSet {
            modifiedLabel.font = UIFont.preferredFont(forTextStyle: .subheadline)
        }
    }
    @IBOutlet weak var idLabel: UILabel!{
        didSet {
            idLabel.font = UIFont.preferredFont(forTextStyle: .caption1)
        }
    }
    
    func setup(thumbUrl: String, name: String, modified: String, id: Int) {
        nameLabel.text = name
        modifiedLabel.text = "Last modify: \(modified.toDate ?? "")"
        idLabel.text = "ID: \(id)"
        guard let url = URL(string: thumbUrl) else { return }
        thumbnailImageView.af_setImage(withURL: url)
        self.selectionStyle = .none
    }
}
