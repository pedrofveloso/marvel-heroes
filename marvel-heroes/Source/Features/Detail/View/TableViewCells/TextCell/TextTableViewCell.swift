//
//  TextTableViewCell.swift
//  marvel-heroes
//
//  Created by Pedro Veloso on 11/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

class TextTableViewCell: UITableViewCell {

    @IBOutlet weak var label: UILabel! {
        didSet {
            label.font = UIFont.preferredFont(forTextStyle: .body)
            label.numberOfLines = 0
            label.lineBreakMode = .byWordWrapping
        }
    }
    
    func setup(text: String) {
        let content = text.isEmpty ? "No content" : text
        self.selectionStyle = .none
        label.text = content
    }
    
    override func prepareForReuse() {
        self.accessoryType = .none
    }
}
