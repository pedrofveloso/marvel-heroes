//
//  DetailViewModelDelegate.swift
//  marvel-heroes
//
//  Created by Pedro Veloso on 11/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

protocol DetailViewModelDelegate: class {
    var hero: Hero { get }
    var sections: [String] { get }
    
    func openURL(for indexPath: IndexPath)
    func numberOfRows(for section: Int) -> Int
    func cell(for indexPath: IndexPath, _ tableView: UITableView) -> UITableViewCell
}
