//
//  DetailViewModel.swift
//  marvel-heroes
//
//  Created by Pedro Veloso on 11/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

class DetailViewModel {
    private let model: Hero
    
    var hero: Hero {
        return model
    }
    
    var sections: [String] {
        return ["", "description", "comics", "series", "urls"]
    }
    
    init(model: Hero) {
        self.model = model
    }
    
    private func heroInfoCell(_ tableView: UITableView, _ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeue(HeroInfoTableViewCell.self, indexPath: indexPath) as? HeroInfoTableViewCell else { return UITableViewCell() }
        cell.setup(thumbUrl: hero.thumbnailUrl, name: hero.name, modified: hero.modified, id: hero.id)
        return cell
    }
    
    private func descriptionCell(_ tableView: UITableView, _ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeue(TextTableViewCell.self, indexPath: indexPath) as? TextTableViewCell else { return UITableViewCell() }
        cell.setup(text: hero.description)
        return cell
    }
    
    private func textCell(_ tableView: UITableView, _ indexPath: IndexPath, text: String) -> UITableViewCell {
        guard let cell = tableView.dequeue(TextTableViewCell.self, indexPath: indexPath) as? TextTableViewCell else { return UITableViewCell() }
        cell.setup(text: text)
        return cell
    }
}

extension DetailViewModel: DetailViewModelDelegate {
    func openURL(for indexPath: IndexPath) {
        if indexPath.section == sections.count - 1 {
            let urlString = hero.urls[indexPath.row].url
            guard let url = URL(string: urlString),
                UIApplication.shared.canOpenURL(url) else { return }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func numberOfRows(for section: Int) -> Int {
        switch section {
        case 0, 1: //HeroInfo and Description
            return 1
        case 2: //Comics
            return hero.comics.count
        case 3: //Series
            return hero.series.count
        default: //URLs
            return hero.urls.count
        }
    }
    
    func cell(for indexPath: IndexPath, _ tableView: UITableView) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            return heroInfoCell(tableView, indexPath)
        case 1:
            return descriptionCell(tableView, indexPath)
        case 2:
            return textCell(tableView, indexPath, text: hero.comics[indexPath.row].name)
        case 3:
            return textCell(tableView, indexPath, text: hero.series[indexPath.row].name)
        default:
            let cell = textCell(tableView, indexPath, text: hero.urls[indexPath.row].type)
            cell.accessoryType = .disclosureIndicator
            return cell
        }
    }
}
