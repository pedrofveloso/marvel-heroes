//
//  HeroInfoURL.swift
//  marvel-heroes
//
//  Created by Pedro Veloso on 11/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import Foundation

struct HeroInfoURL: Codable {
    let type: String
    let url: String
}

extension HeroInfoURL: Equatable {
    static func == (lhs: HeroInfoURL, rhs: HeroInfoURL) -> Bool {
        return lhs.type == rhs.type &&
            lhs.url == rhs.url
    }
}
