//
//  Hero.swift
//  marvel-heroes
//
//  Created by Pedro Veloso on 10/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

struct Hero: Codable {
    let name: String
    let description: String
    let modified: String
    let id: Int
    let thumbnailUrl: String
    let comics: [Item]
    let series: [Item]
    let urls: [HeroInfoURL]
    
    private enum Keys: CodingKey {
        case name, description, modified, thumbnail, id, comics, series, urls
    }
    
    private enum ThumbnailValues: CodingKey {
        case path, `extension`
    }
    
    private enum ItemValues: CodingKey {
        case items
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Keys.self)
        name = try container.decode(String.self, forKey: .name)
        description = try container.decode(String.self, forKey: .description)
        modified = try container.decode(String.self, forKey: .modified)
        id = try container.decode(Int.self, forKey: .id)
        
        let thumbnailContainer = try container.nestedContainer(keyedBy: ThumbnailValues.self, forKey: .thumbnail)
        let thumnailPath = try thumbnailContainer.decode(String.self, forKey: .path)
        let ext = try thumbnailContainer.decode(String.self, forKey: .extension)
        
        thumbnailUrl = "\(thumnailPath).\(ext)"
        
        let comicsContainer = try container.nestedContainer(keyedBy: ItemValues.self, forKey: .comics)
        comics = try comicsContainer.decode([Item].self, forKey: .items)
        
        let seriesContainer = try container.nestedContainer(keyedBy: ItemValues.self, forKey: .series)
        series = try seriesContainer.decode([Item].self, forKey: .items)
        
        urls = try container.decode([HeroInfoURL].self, forKey: .urls)
    }
}

extension Hero: Equatable {
    static func == (lhs: Hero, rhs: Hero) -> Bool {
        return lhs.name == rhs.name &&
            lhs.description == rhs.description &&
            lhs.thumbnailUrl == rhs.thumbnailUrl &&
            lhs.id == rhs.id &&
            lhs.modified == rhs.modified &&
            lhs.comics == rhs.comics &&
            lhs.series == rhs.series &&
            lhs.urls == rhs.urls
    }
}
