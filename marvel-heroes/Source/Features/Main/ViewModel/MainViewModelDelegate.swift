//
//  MainViewModelDelegate.swift
//  marvel-heroes
//
//  Created by Pedro Veloso on 11/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import Foundation

/**
 Responsible for connect the layer between MainViewModel and MainViewController
 * *fetch()* -> asks for viewModel to get data from repository
 * *didSelectHero(at:index)* -> tells the viewModel that a line of tableview were tapped
 */
protocol MainViewModelViewDelegate: class {
    var heroes: [Hero] { get }
    func shouldFetch(for indexPaths: [IndexPath]) -> Bool
    func fetch()
    func didSelectHero(at index: Int)
    func set(view: MainViewControllerDelegate)
}
