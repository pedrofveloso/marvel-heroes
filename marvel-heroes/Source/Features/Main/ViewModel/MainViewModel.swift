//
//  MainViewModel.swift
//  marvel-heroes
//
//  Created by Pedro Veloso on 11/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

class MainViewModel {
    //MARK : - Dependencies
    private let coordinator: CoordinatorDelegate
    private let repository: RepositoryDelegate
    private weak var view: MainViewControllerDelegate?
    private let OFFSET_FETCH = 20
    private(set) var fetchPage: Int = 0
    
    //MARK: - Properties
    var isFetching: Bool = false {
        willSet {
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = newValue
            }
        }
    }
    private var heroesInfo: ResponseWrapper<Hero>?
    
    var heroes: [Hero] {
        return heroesInfo?.results ?? [Hero]()
    }
    
    init(repository: RepositoryDelegate, coordinator: CoordinatorDelegate) {
        self.repository = repository
        self.coordinator = coordinator
    }
}

extension MainViewModel: MainViewModelViewDelegate {
    func shouldFetch(for indexPaths: [IndexPath]) -> Bool {
        guard let heroes = heroesInfo?.results,
            let totalHeroesApi = heroesInfo?.total,
            let index = indexPaths.last?.row else { return false }
        return (index >= heroes.count - 1) && (heroes.count - 1 <= totalHeroesApi)
    }
    
    func fetch() {
        guard !isFetching else { return }
        isFetching = true
        repository.fetch(offset: OFFSET_FETCH * fetchPage,
                         success: { [weak self] response in
            guard let self = self else { return }
            
            if self.heroesInfo == nil {
                self.heroesInfo = response
            } else {
                self.heroesInfo?.results.append(contentsOf: response.results)
            }
            
            self.view?.reloadData()
            self.isFetching = false
            self.fetchPage += 1
        }) { error in
            self.view?.showAlert(message: error.domain)
            self.isFetching = false
        }
    }
    
    func didSelectHero(at index: Int) {
        let vc = DetailViewController(nibName: "\(DetailViewController.self)", bundle: nil)
        vc.viewModel = DetailViewModel(model: heroes[index])
        coordinator.push(viewController: vc)
    }
    
    func set(view: MainViewControllerDelegate) {
        self.view = view
    }
}
