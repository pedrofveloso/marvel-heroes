//
//  MainRepository.swift
//  marvel-heroes
//
//  Created by Pedro Veloso on 11/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import Foundation

typealias HeroesListSuccess = (_: ResponseWrapper<Hero>) -> Void

protocol RepositoryDelegate: class {
    func fetch(offset: Int, success: @escaping HeroesListSuccess, failure: @escaping Failure)
}

class Repository {
    private func fetchFromInternet(offset: Int, success: @escaping Success,
                                   failure: @escaping Failure) {
        Network<Hero>.execute(request: HeroesListRequest(offset: offset), success: { response in
            success(response)
        }) { error in
            failure(error)
        }
    }
}

extension Repository: RepositoryDelegate {
    
    func fetch(offset: Int, success: @escaping HeroesListSuccess, failure: @escaping Failure) {
        //Here we can put some logic to determine the local we would to fetch the data:
        //Internet, CoreData, UserDefaults, Json in documents folder...
        //If we need to add, delete or change the local, it is transparent to ViewModel layer
        fetchFromInternet(offset: offset, success: { response in
            guard let response = response as? ResponseWrapper<Hero> else {
                failure(NSError.generalError)
                return
            }
            success(response)
        }) { (error) in
            failure(error)
        }
    }
}
