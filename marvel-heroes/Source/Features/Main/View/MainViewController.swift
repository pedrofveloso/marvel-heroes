//
//  MainViewController.swift
//  marvel-heroes
//
//  Created by Pedro Veloso on 09/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    var viewModel: MainViewModelViewDelegate? {
        didSet {
            viewModel?.set(view: self)
        }
    }
    
    //IBOutlets
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
            tableView.prefetchDataSource = self
            tableView.register(MainTableViewCell.self)
        }
    }
    
    //MARK: - Initializers
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Marvel heroes"
        navigationItem.largeTitleDisplayMode = .always
        viewModel?.fetch()
    }

}

extension MainViewController: MainViewControllerDelegate {
    func reloadData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func showAlert(message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Oh, no!", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension MainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.heroes.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeue(MainTableViewCell.self, indexPath: indexPath) as? MainTableViewCell,
            let hero = viewModel?.heroes[indexPath.row] else {
            return UITableViewCell()
        }
        cell.setup(hero: hero)
        return cell
    }
}

extension MainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel?.didSelectHero(at: indexPath.row)
    }
}

extension MainViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        guard let viewModel = viewModel,
            viewModel.shouldFetch(for: indexPaths) else { return }
        
        viewModel.fetch()
    }
}
