//
//  MainViewControllerDelegate.swift
//  marvel-heroes
//
//  Created by Pedro Veloso on 11/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import Foundation

protocol MainViewControllerDelegate: class {
    func reloadData()
    func showAlert(message: String)
}
