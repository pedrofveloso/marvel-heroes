//
//  MainTableViewCell.swift
//  marvel-heroes
//
//  Created by Pedro Veloso on 09/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit
import AlamofireImage

class MainTableViewCell: UITableViewCell {
    @IBOutlet weak var thumbnailImageView: UIImageView! {
        didSet {
            thumbnailImageView.layer.cornerRadius = thumbnailImageView.frame.height/2
            thumbnailImageView.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.font = UIFont.preferredFont(forTextStyle: .headline)
        }
    }
    @IBOutlet weak var subtitleLabel: UILabel! {
        didSet {
            subtitleLabel.font = UIFont.preferredFont(forTextStyle: .caption2)
        }
    }

    func setup(hero: Hero) {
        thumbnailImageView.image = UIImage()
        titleLabel.text = hero.name
        subtitleLabel.text = "Last modify: \(hero.modified.toDate ?? "-")"
        
        guard let url = URL(string: hero.thumbnailUrl) else { return }
        
        thumbnailImageView?.af_setImage(withURL: url)
    }
    
    override func prepareForReuse() {
        thumbnailImageView.image = UIImage()
    }
    
}
