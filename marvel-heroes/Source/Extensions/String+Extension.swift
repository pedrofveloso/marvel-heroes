//
//  String+Extension.swift
//  marvel-heroes
//
//  Created by Pedro Veloso on 10/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import Foundation
import CommonCrypto

extension String {
    var md5: String {
        let messageData = self.data(using:.utf8)!
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        return digestData.map { String(format: "%02hhx", $0) }.joined()
    }
    
    var toDate: String? {
        let dateStr = String(self.prefix(10))
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        df.timeZone = TimeZone.current
        df.locale = Locale.current
        guard let date = df.date(from: dateStr) else { return nil }
        df.dateFormat = "dd/MM/yyyy"
        return df.string(from: date)
    }
}
