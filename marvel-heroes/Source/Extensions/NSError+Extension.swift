//
//  NSError+Extension.swift
//  marvel-heroes
//
//  Created by Pedro Veloso on 10/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import Foundation

extension NSError {
    static var noInternetError: NSError {
        return NSError(domain: "You have no internet, please try again.", code: 001, userInfo: nil)
    }
    
    static var generalError: NSError {
        return NSError(domain: "Something wrong is not right. Please retry and pray to Thor to it works this time.", code: 002, userInfo: nil)
    }
}
