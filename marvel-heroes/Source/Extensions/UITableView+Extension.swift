//
//  UITableView+Extension.swift
//  marvel-heroes
//
//  Created by Pedro Veloso on 11/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

extension UITableView {
    
    func register<T:UITableViewCell>(_ : T.Type) {
        self.register(UINib(nibName: "\(T.self)", bundle: nil), forCellReuseIdentifier: "\(T.self)")
    }
    
    func dequeue<T:UITableViewCell>(_ : T.Type, indexPath: IndexPath) -> UITableViewCell? {
        return self.dequeueReusableCell(withIdentifier: "\(T.self)", for: indexPath)
    }
}
