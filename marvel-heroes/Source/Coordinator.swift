//
//  Coordinator.swift
//  marvel-heroes
//
//  Created by Pedro Veloso on 11/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

protocol CoordinatorDelegate: class {
    func push(viewController: UIViewController)
}

class Coordinator {
    let navigation: UINavigationController
    var viewController: UIViewController?
    
    init(navigation: UINavigationController) {
        self.navigation = navigation
    }
}

extension Coordinator: CoordinatorDelegate {
    func push(viewController: UIViewController) {
        navigation.pushViewController(viewController, animated: true)
    }
}
