//
//  Requestable.swift
//  marvel-heroes
//
//  Created by Pedro Veloso on 10/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import Foundation
import Alamofire

protocol Requestable: class {
    var url: String { get }
    var method: HTTPMethod { get }
    var parameters: [String: Any] { get }
    var headers: [String: String] { get }
    var encoding: ParameterEncoding { get }
}
