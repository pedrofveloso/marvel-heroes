//
//  ResponseWrapper.swift
//  marvel-heroes
//
//  Created by Pedro Veloso on 11/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import Foundation

struct ResponseWrapper<T:Codable>: Codable {
    let offset: Int
    let total: Int
    let count: Int
    var results: [T]
    
    private enum ParentKey: CodingKey {
        case data
    }
    
    private enum Keys: CodingKey {
        case offset, total, count, results
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ParentKey.self)
        let nestedContainer = try container.nestedContainer(keyedBy: Keys.self, forKey: .data)
        offset = try nestedContainer.decode(Int.self, forKey: .offset)
        total = try nestedContainer.decode(Int.self, forKey: .total)
        count = try nestedContainer.decode(Int.self, forKey: .count)
        results = try nestedContainer.decode([T].self, forKey: .results)
    }
}
