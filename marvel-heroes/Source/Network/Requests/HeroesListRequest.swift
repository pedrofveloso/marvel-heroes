//
//  HeroesListRequest
//  marvel-heroes
//
//  Created by Pedro Veloso on 10/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import Foundation
import Alamofire

class HeroesListRequest: Requestable {
    
    let offset: Int
    
    init(offset: Int) {
        self.offset = offset
    }
    
    var url: String {
        return "https://gateway.marvel.com:443/v1/public/characters"
    }
    var method: HTTPMethod {
        return .get
    }
    var parameters: [String : Any] {
        let ts = Int(Date().timeIntervalSince1970)
        let hash = "\(ts)\(privateKey)\(publicKey)".md5
        return [
            "offset" : offset,
            "ts": "\(ts)",
            "apikey": publicKey,
            "hash": hash
        ]
    }
    var headers: [String : String] {
        return [:]
    }
    var encoding: ParameterEncoding {
        return URLEncoding.queryString
    }
}
