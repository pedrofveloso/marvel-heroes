//
//  AppDelegate.swift
//  marvel-heroes
//
//  Created by Pedro Veloso on 09/05/19.
//  Copyright © 2019 pedrofveloso. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder {

    var window: UIWindow?

    private func setupWindow() {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = setupRootViewController()
        window?.makeKeyAndVisible()
    }
    
    private func setupRootViewController() -> UINavigationController {
        let mainViewController = MainViewController(nibName: "\(MainViewController.self)", bundle: nil)
        let navigationController = UINavigationController(rootViewController: mainViewController)
        let coordinator = Coordinator(navigation: navigationController)
        let viewModel = MainViewModel(repository: Repository(), coordinator: coordinator)
        mainViewController.viewModel = viewModel
        return navigationController
    }
}

extension AppDelegate: UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupWindow()
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {}
    
    func applicationDidEnterBackground(_ application: UIApplication) {}
    
    func applicationWillEnterForeground(_ application: UIApplication) {}
    
    func applicationDidBecomeActive(_ application: UIApplication) {}
    
    func applicationWillTerminate(_ application: UIApplication) {}
}
